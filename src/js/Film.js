import { BASE_URL, FILM_PATTERN, KEY } from './constants'
import { form, filmMore } from './index'

export default class Film {
    constructor(descr) {
        this.id = descr.id
        this.poster = (descr.poster !== 'N/A') ? descr.poster : 'img/placeholder.png'
        this.title = (descr.title !== 'N/A') ? descr.title : 'No title'
        this.year = (descr.year !== 'N/A') ? descr.year.match(/\d{4}/)[0] : 'No year'
        this.plot = ''
        this.favourite = descr.favourite
        this.toggleFavouriteBtn = {}
    }

    createFilmNode () {
        const film = FILM_PATTERN.cloneNode(true)
        this.toggleFavouriteBtn = film.querySelector('.favourite')
        this.toggleFavouriteBtn.innerHTML = this.favourite ? 'Dislike' : 'Like'
        this.toggleFavouriteBtn.onclick = this.toggleFavourite.bind(this) 
        film.classList.remove('hidden', 'film-pattern')

        film.getElementsByTagName('img')[0].src = this.poster
        film.getElementsByTagName('h2')[0].innerHTML = this.title
        film.getElementsByTagName('h2')[0].onclick = this.open.bind(this)
        film.getElementsByTagName('p')[0].innerHTML = this.year
        return film
    }

    toggleFavourite() {
        
        const existing = JSON.parse(localStorage.getItem(KEY) || '{}')
        if (this.favourite) {
            this.favourite = false
            this.toggleFavouriteBtn.innerHTML = 'Like'
            if (!existing[this.id]) return
            delete existing[this.id]
            if (!Object.keys(existing).length) {
                localStorage.removeItem(KEY)
                return
            }
            
        } else {
            this.favourite = true
            this.toggleFavouriteBtn.innerHTML = 'Dislike'
            if (existing[this.id]) return
            existing[this.id] = {
                Poster: this.poster,
                Title: this.title,
                Year: this.year,
                favourite: true
            }
        }
        localStorage.setItem(KEY, JSON.stringify(existing)) 
    }

    async open() {
        const queryParams = {
            i: this.id,
            apikey: 'd5677312'
        }
        const query = form.encodeParams(queryParams)
        const requestURL = `${BASE_URL}/?${query}`

        try {
            let data = await form.request(requestURL)
            this.plot = (data.Plot !== 'N/A') ? data.Plot : '',
            filmMore.open(this)
        } catch(err) {
            if (err.Error) this.error(err.Error)
            else {
                this.error('Sorry, something wents wrong.')
                new Error(err)
            }
        }
    }
}