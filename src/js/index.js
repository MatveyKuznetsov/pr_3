import '../css/style.css'

import Form from './Form'
import FilmMore from './FilmMore'
import Results from './Results'

export const form = new Form()
export const filmMore = new FilmMore()
export const results = new Results()
