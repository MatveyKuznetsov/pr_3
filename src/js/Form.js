import { BASE_URL } from './constants'
import { results } from './index'

export default class Form {
    
    constructor() {
        this.status = document.querySelector('.error')
        this.field = document.querySelector('.field .input')
        this.searchBtn = document.querySelector('.searchBtn')
        
        this.searchBtn.onclick = this.search.bind(this)
        window.onkeydown = e => e.keyCode === 13 ? this.search.bind(this)() : null
        this.status.classList.add('hidden')
    }

    getStatus() {
        return this.status
    }

    encodeParams(params) { 
        return Object.keys(params).map(key => 
            [key, params[key]].map(encodeURIComponent).join('=')
        ).join('&')
    }

    async request(requestURL) {
        const responce = await fetch(requestURL)
        const result = await responce.json()
        if (result.Response === "False") throw result
        return result
    }

    error(message) {
        results.filmsBox.innerHTML = ''
        this.status.classList.remove('hidden')
        this.status.innerHTML = message
        throw new Error(message)
    }

    async search() {
        if (!this.field.value.trim()) return
        const queryParams = {
            s: this.field.value,
            apikey: 'd5677312'
        }
        const query = this.encodeParams(queryParams)
        const requestURL = `${BASE_URL}/?${query}`
        
        try {
            const data = await this.request(requestURL)
            results.render(data.Search)
        } catch(err) {
            if (err.Error) this.error(err.Error)
            else {
                console.error(err)
                this.error('Sorry, something wents wrong :(')
            }
        }
    }
}