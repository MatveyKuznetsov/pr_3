export default class FilmMore {
    constructor () {
        this.pattern = document.querySelector('.current-film__wr')
        this.favouriteBtn = this.pattern.querySelector('.favourite')

        this.pattern.querySelector('.back').onclick = this.close.bind(this)
    }
    
    open(film) {
        this.pattern.getElementsByTagName('img')[0].src = film.poster
        this.pattern.getElementsByTagName('h2')[0].innerHTML = film.title
        this.pattern.getElementsByTagName('p')[0].innerHTML = film.year
        this.pattern.getElementsByTagName('p')[1].innerHTML = film.plot

        document.body.classList.add('cut')
        this.pattern.classList.remove('hidden')

        this.favouriteBtn.innerHTML = film.favourite ? 'Dislike' : 'Like'
        this.favouriteBtn.onclick = () => {
            this.favouriteBtn.innerHTML = film.favourite ? 'Like' : 'Dislike'
            film.toggleFavourite.bind(film)()
        }
    }
    
    close() {
        this.pattern.classList.add('hidden')
        document.body.classList.remove('cut')
    }
}