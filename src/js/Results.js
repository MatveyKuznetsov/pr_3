import { KEY } from './constants'
import Film from './Film'
import { form } from './index'

export default class Results {
    constructor() {
        this.filmsBox = document.querySelector('.results .films')
        this.initialRender()
    }

    initialRender() {
        const existing = JSON.parse(localStorage.getItem(KEY) || '{}')
        const array = []
        let i = 0
        for (let key in existing) {
            existing[key].imdbID = key
            array[i] = existing[key]
            i++
        }
        this.render(array)
    }

    render(data) {
        form.getStatus().classList.add('hidden')
        this.filmsBox.innerHTML = ''
        data.forEach(film => {
            const currentFilm = new Film({
                id: film.imdbID,
                poster: film.Poster,
                title: film.Title,
                year: film.Year,
                favourite: !!film.favourite
            })
            this.filmsBox.appendChild(currentFilm.createFilmNode())
        })
    }
}